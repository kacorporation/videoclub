# Software VideoClub

#### Mi Negocio

- Datos
    - Nombre
    - Cuit
    - Dirección
    - Teléfono
    - Mail
- Alta
- Baja
- Modificación
- Guardar
- Cancelar

#### Personal

- Datos
    - Apellido
    - Nombre
    - Dni
    - Teléfono
    - Dirección
    - Fecha ingreso
    - Estado (Activo/Inactivo)
- Buscar
- Alta
- Baja
- Modificación
- Guardar
- Cancelar

### Película

- Datos
    - Código película
    - N° Copia
    - Nombre
    - Director
    - Actor
    - Año
    - Género
    - Costo Venta
    - Costo Alquiler
    - Stock
    - Estado (Disponible/Alquilado/Sin Stock)
- Buscar
- Alta
- Baja
- Modificación
- Guardar
- Cancelar

### Cliente

- Datos
    - Código Cliente
    - Apellido
    - Nombre
    - Dni
    - Teléfono
    - Dirección
    - Estado (Sin Deuda/Con Deuda)
- Buscar
- Alta
- Baja
- Modificación
- Guardar
- Cancelar

### Alquiler

- Generar Comprobante
    - Cliente
    - Pelicula
    - Fecha retiro 
    - Fecha Devolucion
    - Cantidad
    - Descuento
    - Total
- Buscar
- Imprimir
- Guardar
- Eliminar
- Cancelar

### Venta

- Generar Comprobante
    - Tipo (Factura/Nota de crédito)
    - Cliente
    - Pelicula
    - Fecha 
    - Cantidad
    - Descuento
    - Total
- Buscar
- Imprimir
- Guardar
- Eliminar
- Cancelar

### Listados

- Personal
- Película
- Cliente
- Alquiler
- Venta

### Configuración

### Salir
